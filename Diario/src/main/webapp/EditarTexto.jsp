<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"

        import ="java.sql.Connection"        
        import ="java.sql.DriverManager"        
        import ="java.sql.ResultSet"        
        import ="java.sql.Statement"        
        import ="java.sql.SQLException"        
        %>
<html>
    <head>
        <title>Editar nota</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    </head>
    <body>
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand">Diario</a>
        </nav>
        
        <div class="container">
            <div class="row mt-10">
                <div class="col">
                    <%
                    Connection conex = null;
                   PreparedStatement ps;
                   Statement sql = null;
                   String URL ="jdbc:mysql://127.0.0.1/Diario?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
                   String usuario = "root";
                   String contraseña="12345";
                           
                   try{
                       Class.forName("com.mysql.cj.jdbc.Driver");
                       conex = (Connection)DriverManager.getConnection(URL,usuario,contraseña);
                       System.out.println(request.getParameter("idTextos"));
                       ps=conex.prepareStatement( "select * from Textos where idTextos=?");
                       ps.setString(1, request.getParameter("idTextos"));
                       ResultSet data = ps.executeQuery();
                       
                       while(data.next())
                       {
                       %>
                        <form action="EditarTextoBD.jsp">
                            <input hidden name="idTextos" value="<%=data.getString("idTextos")%>">
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Texto</label>
                                <textarea class="form-control"  rows="3" name="texto" ><%=data.getString("Texto")%></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Calificacion</label>
                                <select class="form-control"  name="calificacion" value="<%=data.getString("Calificacion")%>">
                                    <option value="Bueno">Bueno</option>
                                    <option value="Regular">Regular</option>
                                    <option value="Malo">Malo</option>
                                </select>
                            </div>
                            <input class="btn btn-success" type="submit" value="Guardar" >
                        </form>
                       <%
                           }
                       data.close();
                       
                   }
                   catch(Exception e)
                   {
                       out.print("Error en la conexión");
                       e.printStackTrace();
                   }

%>

                    
                    
                  
                </div>
            </div>
        </div>

    </body>
            <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</html>
