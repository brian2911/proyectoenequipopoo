<%-- 
    Document   : index
    Created on : 18-may-2020, 21:23:51
    Author     : dcuadritos
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"

        import ="java.sql.Connection"        
        import ="java.sql.DriverManager"        
        import ="java.sql.ResultSet"        
        import ="java.sql.Statement"        
        import ="java.sql.SQLException"        
        %>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <title>Hello, world!</title>
    </head>
    <body>
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand">Diario</a>
            <form class="form-inline">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><a href="AgregarTexto.html">Agregar nota</a></button>
            </form>
        </nav>
        <div class="container">
            <div class="row mt-10">
                <div class="col">
                    <table class="table table-hover">
                        <thead>
                          <tr>

                            <th scope="col">Texto</th>
                            <th scope="col">Calificación</th>
                            <th scope="col">Fecha</th>
                             <th scope="col">Opciones</th>
                              
                          </tr>
                        </thead>
                        <tbody>
<% 
                   Connection conex = null;
                   PreparedStatement ps;
                   Statement sql = null;
                   String URL ="jdbc:mysql://127.0.0.1/Diario?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
                   String usuario = "root";
                   String contraseña="12345";
                           
                   try{
                       Class.forName("com.mysql.cj.jdbc.Driver");
                       conex = (Connection)DriverManager.getConnection(URL,usuario,contraseña);
                       
                       ps=conex.prepareStatement( "select * from Textos where visible=1");
                       ResultSet data = ps.executeQuery();
                       System.out.println(data);
                       while(data.next())
                       {
                       %>
                       <tr>
                           <td style="width: 400px">
                               <% out.print(data.getString("Texto"));%>
                           </td>
                           <td>
                               <% out.print(data.getString("Calificacion"));%>
                           </td>
                           <td>
                               <% out.print(data.getDate("Fecha"));%> <% out.print(data.getTime("Fecha"));%>
                           </td>
                        
                           <td><a href="EditarTexto.jsp?idTextos=<%=data.getInt("idTextos")%>">Editar</a> | <a href="BorrarTexto.jsp?idTextos=<%=data.getInt("idTextos")%>">Borrar</a> </td>
                       </tr>
                       <%
                       }
                       data.close();
                       
                   }
                   catch(Exception e)
                   {
                       out.print("Error en la conexión");
                       e.printStackTrace();
                   }

%>


                        </tbody>
                      </table>
                </div>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
